# Basket

Simple shopping list app built with Vue.js as a code example.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8089
npm run dev

# build for production with minification
npm run build
```
